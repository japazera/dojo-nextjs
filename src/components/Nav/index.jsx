import style from './Nav.module.scss';
import Link from 'next/link';

export default function Nav() {

	return (
		<div className={style.Nav}>
			<div className={style.container + " container"}>
				<div className={style.logo}>LOGO</div>
				<nav>
					<Link href="/">
						<a>Home</a>
					</Link>
					<Link href="/about">
						<a>About</a>
					</Link>
					<Link href="/contact">
						<a>Contact</a>
					</Link>
				</nav>
			</div>
		</div>
	)
}
