# Overview
Este repositório serve de suporte para o dojo de Next.js.

## Instalando Dependências de Ambiente
- [Next.js](https://nextjs.org/docs/getting-started)
- [Node.js](https://nodejs.org/en/download/)
- Yarn

## Iniciando um projeto

Para criar um novo projeto:

```console
yarn create next-app
```

Para começar a desenvolver, mude para o dir do repo e execute:
```console
yarn dev
```

## Estrutura de pastas
- pages
    - sistema de rotas
    - [slug]
- public
- styles
- components

## Import Absoluto
- [Absolute Imports](https://nextjs.org/docs/advanced-features/module-path-aliases)

## Suporte para SASS

```console
yarn add sass
```

- Configurando globals.scss
- Configurando vars.scss

# Sobre o Next.js
- [next/link](https://nextjs.org/docs/api-reference/next/link)
- [next/image](https://nextjs.org/docs/api-reference/next/image)
- [Pre Rendering](https://nextjs.org/docs/basic-features/pages#two-forms-of-pre-rendering)

# Criando componentes

## Componentes React
- Estrutura de pastas
- imports
- props
    - destrutores
- useState
- useEffect

## Componentes Comuns
- Nav
- Footer
- Layout
- Button

## Componente Carousel
- [Embla Carousel](https://www.embla-carousel.com/examples/basic/)

# Deploy
Para fazer o build:
```console
yarn build
```

Para iniciar o app:
```console
yarn start
```

Rodar em background:
```console
yarn start&
```
# Links Úteis
- [Loading Environment Variables](https://nextjs.org/docs/basic-features/environment-variables)
- [Styled JSX](https://nextjs.org/blog/styling-next-with-styled-jsx)

# Proximos Passos
- [LP1 PSEL](https://drive.google.com/drive/folders/1uJznPwXY6QziYF_mZL4jVPW6M-Zrw-6l)
- [LP2 PSEL](https://drive.google.com/drive/folders/11jpfUan2fupHiv0VnPUY5fY2IT1OM6gD)
- Integração com o Wordpress + ACF (WIP)