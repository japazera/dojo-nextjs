import style from './Button.module.scss';
import Link from 'next/link';

export default function Button({ children, external, href, className }) {

	return (
		<>
			{
				external == 'false' ?
					<Link href={href}>
						<a className={style.Button + ' ' + className}>
							{children}
						</a>
					</Link>
					:
					<a href={href} className={style.Button + ' ' + className}>
						{children}
					</a>
			}

		</>
	);
}
