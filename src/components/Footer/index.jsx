import style from './Footer.module.scss';

export default function Footer() {

	return (
		<>
			<div className={style.Footer}>Copyright ©2021 All rights reserved</div>
		</>
	);
}
