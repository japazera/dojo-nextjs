import Layout from 'components/Layout';
import Carousel from 'components/Carousel';

export default function Home() {

	return (
		<>
			<Layout>
				<Carousel />
				<div className="container">
					<h1>Home</h1>
					<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Labore, voluptatem sit, architecto quos atque accusamus corporis iusto doloribus eveniet blanditiis repellendus autem recusandae reprehenderit qui, delectus aspernatur nostrum animi voluptas.</p>
				</div>
			</Layout>
		</>
	)
}
