import Layout from 'components/Layout';

export default function About() {

	return (
		<>
			<Layout>
				<div className="container">
					<h1>About Us</h1>
					<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Animi voluptas.</p>
				</div>
			</Layout>
		</>
	)
}
