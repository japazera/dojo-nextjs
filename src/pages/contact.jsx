import Layout from 'components/Layout';

export default function Contact() {

	return (
		<>
			<Layout>
				<div className="container">
					<h1>Contact</h1>
					<p>Eveniet blanditiis repellendus autem recusandae reprehenderit qui, delectus aspernatur nostrum animi voluptas.</p>
				</div>
			</Layout>
		</>
	)
}
